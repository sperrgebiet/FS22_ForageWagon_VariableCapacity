-- sperrgebiet 2022
-- Inspired by the FS17 version. Kudos to Ziuta and 50keda, as they came up with it initially. Seems the history goes back to FS15 even
--
-- There is a FS19 version on some loader wagons by me. As I still like the idea, I decided to make it a full specialization for FS22 as well
--
-- Unlike others I see the meaning in mods that others can learn, expand and improve them. So feel free to use this in your own mods, 
-- add stuff to it, improve it. Your own creativity is the limit ;) If you want to mention me in the credits fine. If not, I'll live happily anyway :P
-- Yeah, I know. I should do a better job in document my code... Next time, I promise... 
--
-- To add/change the available capacities add the following section in your <modVehicle>.xml
--
-- <variableCapacity>54000, 75000, 100000</variableCapacity>
-- 
-- Add it as specilication and you're good to go.
-- As you can see in the code, it just changes the capacity of the first fillUnit. At a later stage this might get changed. But I think for the majority of use cases,
-- especially for forageWagons this is fine

VariableCapacity = {}
VariableCapacity.ModName = g_currentModName
VariableCapacity.ModDirectory = g_currentModDirectory
VariableCapacity.Version = "1.0.0.0"

VariableCapacity.debug = fileExists(VariableCapacity.ModDirectory ..'debug')

print(string.format('VariableCapacity v%s - DebugMode %s)', VariableCapacity.Version, tostring(VariableCapacity.debug)))

addModEventListener(VariableCapacity)

function VariableCapacity:dp(val, fun, msg) -- debug mode, write to log
	if not VariableCapacity.debug then
		return;
	end

	if msg == nil then
		msg = ' ';
	else
		msg = string.format(' msg = [%s] ', tostring(msg));
	end

	local pre = 'VariableCapacity DEBUG:';

	if type(val) == 'table' then
		--if #val > 0 then
			print(string.format('%s BEGIN Printing table data: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
			DebugUtil.printTableRecursively(val, '.', 0, 3);
			print(string.format('%s END Printing table data: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
		--else
		--	print(string.format('%s Table is empty: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
		--end
	else
		print(string.format('%s [%s]%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
	end
end

function VariableCapacity.initSpecialization()
	local schema = Vehicle.xmlSchemaSavegame
	schema:setXMLSpecializationType("variableCapacity")
	schema:register(XMLValueType.INT, "vehicles.vehicle(?).variableCapacity#capacity", "")
	
	if VariableCapacity.xmlSchema == nil then
		local configSchema = XMLSchema.new("VariableCapacity")
		configSchema.rootNodeName = "VariableCapacities"
		configSchema:setXMLSpecializationType("variableCapacity")
		configSchema:register(XMLValueType.BOOL, ".showHelp", "Show key bindings in F1 Menu")
		configSchema:register(XMLValueType.VECTOR_N, ".loaderWagon(?)", "List of loader wagons")
		configSchema:register(XMLValueType.STRING, ".loaderWagon(?)#configFileNameClean", "ConfigFileName without XML")
		configSchema:register(XMLValueType.STRING, ".loaderWagon(?)#capacities", "Loader wagon capacities")
		
		VariableCapacity.xmlSchema = configSchema
	end
end

function VariableCapacity.prerequisitesPresent(specializations)
    return true;
end

function VariableCapacity.registerEventListeners(vehicleType)
	local functionNames = {	"onRegisterActionEvents", "onDraw", "onLoad", "saveToXMLFile", "onReadStream", "onWriteStream" }
	for _, functionName in ipairs(functionNames) do
		SpecializationUtil.registerEventListener(vehicleType, functionName, VariableCapacity)
	end
end

function VariableCapacity:onRegisterActionEvents(isSelected, isOnActiveVehicle)
	if self.isClient then
		local spec = self.spec_variableCapacity
		
		self:clearActionEventsTable(spec.actionEvents)
		
		if isSelected then
			local _, actionEventId = self:addActionEvent(spec.actionEvents, 'toggleCapacity', self, VariableCapacity.action_toggleCapacity, false, true, false, true, nil)
			g_inputBinding:setActionEventTextVisibility(actionEventId, VariableCapacity.showHelp)
			g_inputBinding:setActionEventTextPriority(actionEventId, GS_PRIO_HIGH)
		end		
	end
end

function VariableCapacity:action_toggleCapacity(actionName, keyStatus, arg3, arg4, arg5)
	
	-- Just go through all the hassle when the object is empty
	if self:getFillUnitFillLevel(1) == 0 then
		-- Actually we should always have a valid capacity set. So make sure that's the case and that we then change to the next capacity from fromg
		local spec = self.spec_variableCapacity
		local capacity = self:getFillUnitCapacity(1)
		local currIndex = VariableCapacity:getIndexByValue(spec.capacities, capacity)
		local nextIndex = 1
		
		if currIndex ~= nil then
			if (currIndex + 1) <= #spec.capacities then
				nextIndex = currIndex + 1
			end
		end
		
		capacity = spec.capacities[nextIndex]
		VariableCapacity.setCapacity(self, capacity)
	else
		g_currentMission:showBlinkingWarning(g_i18n.modEnvironments[VariableCapacity.ModName].texts.WarningNotEmpty, 5000)
	end
end

function VariableCapacity:onDraw(isActiveForInput, isActiveForInputIgnoreSelection, isSelected)
	g_currentMission:addExtraPrintText(g_i18n.modEnvironments[VariableCapacity.ModName].texts.currentCapacity .. string.format(" : %d",  self:getFillUnitCapacity(1)))
end

function VariableCapacity:loadMap(name)
	-- Just load the config on the server
	if g_server ~= nil then
		VariableCapacity.initSpecialization()

		if VariableCapacity.xmlFilename == nil then
			VariableCapacity.userPath = getUserProfileAppPath();
			VariableCapacity.saveBasePath = VariableCapacity.userPath .. 'modSettings/VariableCapacity/';
			createFolder(VariableCapacity.userPath .. 'modSettings/');
			createFolder(VariableCapacity.saveBasePath);
			VariableCapacity.xmlFilename = VariableCapacity.saveBasePath .. 'VariableCapacity.xml';	
			
			VariableCapacity.capacities = {}
			
			--If there is no config we want to create the vanilla one first, and then we're going to load it
			VariableCapacity:writeConfig()
			VariableCapacity:loadConfig()
		end
	end
end

function VariableCapacity:loadConfig()
	if g_server ~= nil then
		VariableCapacity:dp("", "loadConfig","")
		if fileExists(VariableCapacity.xmlFilename) then
			--local xmlFile = loadXMLFile('VariableCapacity.loadFile', self.xmlFilename)
			local xmlFile = XMLFile.load('VariableCapacities', self.xmlFilename, VariableCapacity.xmlSchema)
			
			if xmlFile:hasProperty('VariableCapacities') then
				VariableCapacity:dp("", "loadConfig","xmlFile hasProperty true")
				
				-- First see if we should show the key bindings in the help menu
				VariableCapacity.showHelp = xmlFile:getValue("VariableCapacities.showHelp", true)
				
				xmlFile:iterate("VariableCapacities.loaderWagon", function (_, key)
					VariableCapacity:dp(key, "loadConfig","xmlFile Iterate")
					local loaderCaps = {}
					if xmlFile:getValue(key) ~= false then
						local configFileNameClean = xmlFile:getValue(key .. "#configFileNameClean")
						local savedCaps = xmlFile:getValue(key.. "#capacities") or "10000, 20000, 30000"
						VariableCapacity:dp(savedCaps, "loadConfig", configFileNameClean)
						for str in string.gmatch(savedCaps, '([^,]+)') do
							table.insert(loaderCaps, tonumber(str))
						end
						
						if #loaderCaps > 0 then
							table.sort(loaderCaps)
							VariableCapacity.capacities[configFileNameClean] = loaderCaps
						end
					end
				end)
			end
		end
	end
end

function VariableCapacity:writeConfig()
	if g_server ~= nil then
		-- This is just to create an example config with the existing basegame loader wagons
		if not fileExists(self.xmlFilename) then
			self.saveFile = createXMLFile('VariableCapacity.config', self.xmlFilename, "VariableCapacities")
			setXMLString(self.saveFile, "VariableCapacities.showHelp", "true")
			
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(0)#configFileNameClean", "bossAlpin251")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(0)#capacities", "16100, 21000")
			
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(1)#configFileNameClean", "zelonCFS2501DO")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(1)#capacities", "23000, 39100")
			
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(2)#configFileNameClean", "faro4010D")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(2)#capacities", "26000, 40000")

			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(3)#configFileNameClean", "rapide580V")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(3)#capacities", "35000, 38000")
			
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(4)#configFileNameClean", "cargos9500")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(4)#capacities", "44000, 88000")
			
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(5)#configFileNameClean", "jumbo10020Tridem")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(5)#capacities", "46600, 100000")
			
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(6)#configFileNameClean", "shuttle490S")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(6)#capacities", "49500, 89100")
			
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(7)#configFileNameClean", "magnon530")
			setXMLString(self.saveFile, "VariableCapacities.loaderWagon(7)#capacities", "50000, 97000")

			saveXMLFile(self.saveFile)
		end
	end
end

function VariableCapacity:onLoad(savegame)

	if g_server ~= nil then

		local spec = self.spec_variableCapacity
		if savegame ~= nil then
			local savedCapacity = savegame.xmlFile:getValue(savegame.key..".variableCapacity#capacity")
			if savedCapacity ~= nil then
				VariableCapacity.setCapacity(self, savedCapacity)
			end
		end
		
		if self.xmlFile:hasProperty('vehicle.variableCapacity') then
			local availableCapacities = getXMLString(self.xmlFile.handle, "vehicle.variableCapacity")
			if availableCapacities ~= nil then
				local capacities = {}
				for str in string.gmatch(availableCapacities, '([^,]+)') do
				   table.insert(capacities, tonumber(str))
				end
			
				if #capacities > 0 then 
					table.sort(capacities)
					spec.capacities = capacities
				end
			end
			
			VariableCapacity:dp(self.configFileNameClean, "onLoad", "Load from vehicle XML")
		elseif VariableCapacity.capacities[self.configFileNameClean] ~= nil then
			spec.capacities = VariableCapacity.capacities[self.configFileNameClean]
			VariableCapacity:dp(self.configFileNameClean, "onLoad", "Load config")
		else	
			local currentCapacity = self:getFillUnitCapacity(1)
			spec.capacities = {currentCapacity, currentCapacity * 1.5}
			VariableCapacity:dp(self.configFileNameClean, "onLoad", "Capacity calculation")
		end

	end
end

function VariableCapacity:saveToXMLFile(xmlFile, key)
	xmlFile:setValue(key.."#capacity", self:getFillUnitCapacity(1))
end

function VariableCapacity.setCapacity(obj, capacity)
	if g_client ~= nil then
		obj:setFillUnitCapacity(1, capacity)

		--FillLevelLimiter integration
		if obj.spec_fillLevelLimiter ~= nil then
			obj:setFillUnitLimit(1, capacity)
		end

		-- create fill units
		for _, mapping in ipairs(obj.spec_fillVolume.fillUnitFillVolumeMapping) do
			for _, fillVolume in ipairs(mapping.fillVolumes) do
				fillVolume.fillUnitFactor = fillVolume.fillUnitFactor / mapping.sumFactors
			end
		end	
		
		for _, fillVolume in ipairs(obj.spec_fillVolume.volumes) do
			local fillUnitCap = obj:getFillUnitCapacity(fillVolume.fillUnitIndex)
			local fillVolumeCapacity = fillUnitCap * fillVolume.fillUnitFactor		
			fillVolume.volume = createFillPlaneShape(fillVolume.baseNode, "fillPlane", fillVolumeCapacity, fillVolume.maxDelta, fillVolume.maxSurfaceAngle, fillVolume.maxPhysicalSurfaceAngle, fillVolume.maxSurfaceDistanceError, fillVolume.maxSubDivEdgeLength, fillVolume.syncMaxSubDivEdgeLength, fillVolume.allSidePlanes, fillVolume.retessellateTop)						
			setVisibility(fillVolume.volume, false)
			for i=#fillVolume.deformers, 1, -1 do
				local deformer = fillVolume.deformers[i]
				deformer.polyline = findPolyline(fillVolume.volume, deformer.posX, deformer.posZ)
				if deformer.polyline == nil and deformer.polyline ~= -1 then
					print("Warning: Could not find 'polyline' for '"..tostring(getName(deformer.node)).."' in '"..obj.configFileName.."'")
					table.remove(fillVolume.deformers, i)
				end
			end		
			link(fillVolume.baseNode, fillVolume.volume)
			
			local fillVolumeMaterial = g_materialManager:getBaseMaterialByName("fillPlane")

			if fillVolumeMaterial ~= nil then
				setMaterial(fillVolume.volume, fillVolumeMaterial, 0)
				g_fillTypeManager:assignFillTypeTextureArrays(fillVolume.volume, true, true, true)
			else
				Logging.error("Failed to assign material to fill volume. Base Material 'fillPlane' not found!")
			end

			fillPlaneAdd(fillVolume.volume, 1, 0, 1, 0, 11, 0, 0, 0, 0, 11)

			fillVolume.heightOffset = getFillPlaneHeightAtLocalPos(fillVolume.volume, 0, 0)

			fillPlaneAdd(fillVolume.volume, -1, 0, 1, 0, 11, 0, 0, 0, 0, 11)
		end
	end
end

function VariableCapacity:getIndexByValue(tbl, val)
	for k, v in ipairs(tbl) do
		if v == val then
			return k
		end
	end
end

--
-- MP Stuff
--
-- Called on client side on join for specialization
function VariableCapacity:onReadStream(streamId, connection)
	if streamReadBool(streamId) then
		local spec = self.spec_variableCapacity
		local caps = streamReadString(streamId)
		local loaderCaps = {}
		for str in string.gmatch(caps, '([^,]+)') do
			table.insert(loaderCaps, tonumber(str))
		end			
		table.sort(loaderCaps)
		spec.capacities = loaderCaps
		VariableCapacity.setCapacity(self, streamReadInt32(streamId))
	end
end


-- Called on server side on join for specialization
function VariableCapacity:onWriteStream(streamId, connection)
	local spec = self.spec_variableCapacity
	
	streamWriteBool(streamId, spec.capacities ~= nil)
	
	if spec.capacities ~= nil then
		streamWriteString(streamId, table.concat(spec.capacities, ","))
		streamWriteInt32(streamId, self:getFillUnitCapacity(1))
	end
end


VariableCapacityEvent = {}
local VariableCapacityEvent_mt = Class(VariableCapacityEvent, Event)

InitEventClass(VariableCapacityEvent, "VariableCapacityEvent")

function VariableCapacityEvent.emptyNew()
    local self = Event.new(VariableCapacityEvent_mt)
	self.className = "VariableCapacityEvent"
    return self
end

function VariableCapacityEvent.new(vehicle, capacity)
    local self = VariableCapacityEvent:emptyNew()
	self.vehicle = vehicle	
    self.capacity = capacity
    return self
end

function VariableCapacityEvent:writeStream(streamId, connection)
	NetworkUtil.writeNodeObject(streamId, self.vehicle)
	streamWriteUInt16(streamId, self.capacity)
end

function VariableCapacityEvent:readStream(streamId, connection)
	self.vehicle = NetworkUtil.readNodeObject(streamId)
	self.capacity = streamReadUInt16(streamId, connection)

	self:run(connection)
end

function VariableCapacityEvent:run(connection)
	--StonePickerStopper:sendNotification(self.text, true)
	if self.vehicle ~= nil then
		VariableCapacity.setCapacity(self.vehicle, self.capacity)
	end
end

function VariableCapacityEvent.sendEvent(vehicle, capacity, noEventSend)
	if noEventSend == nil or noEventSend == false then
		if g_server ~= nil then
			g_server:broadcastEvent(VariableCapacityEvent.new(vehicle, capacity))
		else
			g_client:getServerConnection():sendEvent(VariableCapacityEvent.new(vehicle, capacity))
		end
	end
end
